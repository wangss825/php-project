<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 2018-11-25
 * Time: 2:39 AM
 */
require_once 'DBAccess\Model.php';
require_once 'DBAccess\Column.php';
use DBAccess\ColumnType;
class Member extends \DBAccess\Model
{
    public $email;
    /**
     * @return bool if the email address is valid
     */
    public function validate_email()
    {
        return
            $this->checkProperty('email', !empty($this->email), 'The %s  can not be empty')
            && $this->checkProperty('email', filter_var($this->email, FILTER_VALIDATE_EMAIL), ' The %s must be valid email format');
    }
    public $password;
    /**
     * @return bool -if the password is valid
     */
    public function validate_password()
    {
        return
            $this->checkProperty('password', !empty($this->password), 'The %s  can not be empty')
            && $this->checkProperty('password',  strlen($this->password)>= 8, ' The %s should be at least 8 characters')
            && $this->checkProperty('password',  strlen($this->password)<=20, ' The %s should be 20  characters or less')
            || $this->checkProperty('passsword',$this-> hashedPassword === $this->password,'Something went wrong with  %s');
    }
    public $membershipType;
    /**
     * @return bool-if the memberType is valid
     */
    public function validate_membershipType()
    {
        return  $this->checkProperty('membershipType', !empty($this->membershipType), 'The %s  can not be empty')
            &&$this->checkProperty('membershipType',  $this->membershipType=='free'
                || $this->membershipType=='paid'|| $this->membershipType=='admin', 'The %s  must be free, paid or admin');
    }
    public $firstName;
    /**
     * @return bool- if the firstName is valid
     */
    public function validate_firstName()
    {
        return $this->checkProperty('firstName',strlen($this->firstName)<=40, 'The %s must 40 characters or less');
    }
    public $lastName;
    /**
     * @return bool- if the lastname is valid
     */
    public function validate_lastName()
    {
        return $this->checkProperty('lastName',strlen($this->lastName)<=40, 'The %s must 40 characters or less');
    }

    public $address1;

    /**
     * @return bool- if address1 is valid
     */
    public function validate_address1()
    {
        return $this->checkProperty('address1',strlen($this->address1)<=500, 'The %s must 500 characters or less');
    }
    public $address2;
    /**
     * @return bool- if address2 is valid
     */
    public function validate_address2()
    {
        return $this->checkProperty('address2',strlen($this->address2)<=500, 'The %s must 500 characters or less');
    }
    public $city;
    /**
     * @return bool- if the city is valid
     */
    public function validate_city()
    {
        return $this->checkProperty('city',strlen($this->city)<=40, 'The %s must 40 characters or less');
    }

    public $province;
    /**
     * @return bool- if the province is valid
     */
    public function validate_province()
    {
        return $this->checkProperty('province',strlen($this->province)<=40, 'The %s must 40 characters or less');
    }

    private  $hashedPassword;

    /**
     * @param $password- the password user entered
     */
    public function sethashedPassword($password)
    {
        $this->hashedPassword = password_hash($password,PASSWORD_DEFAULT);
        $this->password = $this->hashedPassword;
    }

    /**
     * Member constructor.
     * @param $email-email address as primary key
     * @param $password-required
     * @param $firstName
     * @param $lastName
     * @param $address1
     * @param $address2
     * @param $city
     * @param $province
     * @param $membershipType-required
     */
    public function __construct($email= null, $password=null, $membershipType=null, $firstName=null, $lastName=null, $address1=null, $address2=null, $city=null, $province=null )
    {
        $this->email = $email;
        $this->password = $password;
        $this->membershipType = $membershipType;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->city = $city;
        $this->province = $province;


        //add Columns
        $this->addTableColumn('email', ColumnType::VRC, null, false, true, false);
        $this->addTableColumn('password', ColumnType::VRC, 255, false);
        $this->addTableColumn('firstName', ColumnType::VRC, 40);
        $this->addTableColumn('lastName', ColumnType::VRC, 40);
        $this->addTableColumn('address1', ColumnType::VRC, 500);
        $this->addTableColumn('address2', ColumnType::VRC, 500);
        $this->addTableColumn('city', ColumnType::VRC, 40);
        $this->addTableColumn('province', ColumnType::VRC, 40);
        $this->addTableColumn('membershipType', ColumnType::VRC, 40, false);

        //add display labels
        $this->addDisplayLabel('email ',"Email");
        $this->addDisplayLabel('password ',"Password");
        $this->addDisplayLabel('firstName ',"First Name");
        $this->addDisplayLabel('lastName ',"Last Bame");
        $this->addDisplayLabel('address1 ',"Address1");
        $this->addDisplayLabel('address2 ',"Address2");
        $this->addDisplayLabel('city ',"City");
        $this->addDisplayLabel('province ',"Province");
        $this->addDisplayLabel('membershipType ',"Membership Type");

        //add Inputs
        $this->addInput('email',\HTMLTools\InputType::TXT);
        $this->addInput('password',\HTMLTools\InputType::PWD);
        $this->addInput('province',\HTMLTools\InputType::SEL,['SK'=>"Saskatoon",'ON'=>'Ontario','AB'=>'Alberta']);
        $this->addInput('membershipType',\HTMLTools\InputType::SEL,["free"=>'free','paid'=>'paid'], '', '','','','','...','admin');
        $this->addInput('firstName',\HTMLTools\InputType::TXT);
        $this->addInput('lastName',\HTMLTools\InputType::TXT);
        $this->addInput('address1',\HTMLTools\InputType::TXT);
        $this->addInput('address2',\HTMLTools\InputType::TXT);
        $this->addInput('city',\HTMLTools\InputType::TXT);




    }


}