<?php
 
namespace DBAccess;
//automatically figure  out which php file to required_once
spl_autoload_register(function ($model)
{
    //look for the php file once folder above (i.e the lib folder)
   require_once '..\\'.$model. '.php';
});

/**
 * User: cst207 Dan LIu, cst201 Dylan Anderson
 * Date: 10/19/2018
 * SERVER LOGIC BLOCK
 *
 * Purpose of this class is to use model type objects and perform SQL commands
 * by just using the intrinsic information about the Model
 *
 * This class extends the builtin sqlite library
 */

class DBObject extends \SQLite3
{

    /**
     * @param Model $model - the object that extends Model
     * @return bool - whether the sql command succeeded or failed
     */
        public function createTable(Model $model)
        {
        //check if the $model use getTableColumn method get an empty array, Then we will set the column otherwise
        if(empty($model->getTableColumns()))
        {
            //use get_object_vars to get the properties of the objects, and store in array
            //use the array_key to get the key of the array, in our case, it is the $model's properties name
            $props = array_keys(get_object_vars($model));
            //loop through the properties, try to add every element a type, in this case, it is varchar, that means text.
            foreach( $props as $value)
            {
                $value .=" NVARCHAR";//set the column type to TEXT
            }
        }
        //get the column sql string, including the empty array, we will use the column sql statement above,
        //also if it is not empty, we will just use the getTableColumn method to get the sql column statement
        $finalColumn = empty($model->getTableColumns())? $props: $model->getTableColumns();

        $sql = '';
        $sql = 'create table  IF NOT EXISTS '.$model->getTableName() .'('.PHP_EOL;
        $sql .= implode(','.PHP_EOL, $finalColumn ) .')';//this line, we use the $final column we just got above.
        $stmt = $this->prepare($sql);

        $result = $stmt->execute();
        $stmt->close();
        return $result ? true: false;
        }


    /**
     * @param Model Model-$model- an object that extends the model class
     * @return bool- whether the sql statement
     */
    public function insert(Model $model)
    {
        if(!$model->validate()){return  false;}// model is not valid-do not insert


        $tbl = $model->getTableName();//the table name
        //get the model's property  names

        //get the model's property names - assoc array of class property name of value-must remove autoincrementing columns;
        $props = array_diff_key(get_object_vars($model), $model->getAutoColumns());
        //$props = get_object_vars($model);//assoc array of class property names/values

        //repeat "?"  for however may items in the $props array- the last question mark should not be followed by a ',';
        $qMarks = rtrim(str_repeat('?,', count($props)),',');

        //get the props array into a comma separated string
        $fieldList= implode(',', array_keys($props));

        //create prepared statment
        $stmt = $this->prepare("insert into $tbl ($fieldList) values ($qMarks)" );

        //bind values
        $i =1;
        foreach($props as $prop=>$val)
        {
            $stmt->bindValue($i, $val,$model->getBindType($prop));
            //$stmt->bindValue($i, $val,SQLITE3_BOTH);

            $i++;
        }
        $result = $stmt->execute();
        // add code to set the autoincremented value from the database- for now just the primary key
        if($result  && in_array($model->getPKFieldName(),$model->getAutoColumns()))
        {
            $pk = $model->getPKFieldName();// in the car example pk is 'id';
            //using a $ after the arrow-> resolves as "$model->id"
            $model->$pk = $this->lastInsertRowID();//set the auto number to the model's primary  key
        }

        $stmt->close();
        return $result? true: false;
    }
    //ex24 remove autoincrementing columns from the $progs array-> user array_diff_key()
    //-in model figure out which columns are auto incrementing -store in private arrary
    //-add getter


    /**
     * @param $model/string- the class  or the name of the class that extends Model and provides the table name for the select query
     * @param Filter[] $filters-array of Filter objects which generate the where clause for the query
     * @param bool $isExclusiveFilter- whether to treat the filters as exclusie (and)  or inclusive(or) in the where clause
     * @param $orderBy - The column to use for ordering - DEFAULT Primary Key column
     * @param $isAscend - whether the order by is Ascending or Descending -DEFAULT ascending
     * @return array- array of model object - the same type as the passed in $mode parameter
     */
    public function selectSome(Model $model,   $filters = array(), $isExclusiveFilter = true, $orderBy='', $isAscend = true)
    {
        $modelArray = array();
        //if$model is a string then assume it is a class name
        if(is_string($model))
        {
            $className = $model;//move the class name to a new variable
            $model = new $className();//instantiate an instance of the object with  the class name.
        }
        else{
            $className = $model->getTableName(); // get the classname/table name from the model object
        }
        //fieldList from the model
        $fieldArray = array_keys(get_object_vars($model));
       // $fieldList = implode(',', array_keys(get_object_vars($model)));
        $fieldList = implode(',', $fieldArray);

        //decide whether we use the And or the OR to join the filters in the  filters
        $join  = $isExclusiveFilter ? ' AND ' : ' OR ';
        // make filter string joined by the operator - but only if there are filters
         $filtersList = empty($filters) ? '1=1' :implode($join,$filters);

         //adds whether the order by will be ASC or DESC in the SQL
         $ascOrDesc = $isAscend ? " ASC": " DESC";
         //adds which field will be used to order by
         $orderField = empty($orderBy) || !in_array($orderBy,$fieldArray) ? $model->getPKFieldName(): $orderBy;
         //if passed in field is not a column and there is no PK then no order by can occur
        //else field does not exist and there is PK use Pk to order by
        //else filed does exist use passed in filed
         $orderField = !in_array($orderBy,$fieldArray) && empty($model->getPKFieldName())? "":$orderField;
         $orderField = !empty($orderField) ? ' order by '.$orderField.$ascOrDesc : "";

        //create prepared statment
            //create the where clause- if needed
        $stmt = $this->prepare("select $fieldList from $className  where $filtersList $orderField ");
        //bind the values
        $i= 1;
        foreach($filters as $objFilt)
        {
            $stmt->bindValue($i,$objFilt->value,$model->getBindType($objFilt->fieldName));
            //$stmt->bindValue($i,$objFilt->value,SQLITE3_BOTH);
            $i++;
        }
        $result = $stmt->execute();
        //loop throuth the results
            //add new model object to the modelArray
        if($result)
        {
            while ($rowArray = $result->fetchArray(SQLITE3_ASSOC)) {
                //need to create a new model object - but we don't know what type  of  object  do we ?
                $item = new $className();// make a new object from the class name stored inside the $className variable
                foreach ($rowArray as $col => $val) {

                    $item->$col = $val;
                }
                $modelArray[] = $item;
            }
        }
        else {
            return false; // if statement failed we should let  the  developer know that the it failed  by returning false
            //if we return an empty array the developer may  think the statement succeded but there were not row in tghe table
            // that met the criteria/filter
        }
        $stmt->close();
        return $modelArray;
    }


    //TODO update

    /**find the row with the same primaty as the model and update the values of the row from model object
     * remember each property in the model has a column  with  the same name in the table
     * @param Model $model- the class that extends the model class
     * @string $pivotField = only used if no primaty key is defined- lets the developer specify which field to filter on
     * @return bool-whether the update succedd(true0 or failed(false)
     */
    public function update($model, $pivotField = '')
    {
        //check if primaty key is defined or prvot field is passed in  otherwise return false
        //and check that the model is valid- if not  then return false - and leave
        if((empty($model->getPKFieldName()) && empty($pivotField)) || !$model->validate())
        {
            return false;// can't do a query so return false.
        }
        $pivotField=empty($model->getPKFieldName())? $pivotField:$model->getPKFieldName();

        $pivotValue = $model->$pivotField;

        $tbl=$model->getTableName();

        //EX26- create the set string to include in our query
        $props = array_diff_key(get_object_vars($model), $model->getAutoColumns());
        //remove primaty key from props array-remember that the property name is the key in the $props array
        unset($props[$pivotField]);
        //unset($props[$model->getPKFieldName()]);//unset will remove from array
        $setString = implode('=?,'.PHP_EOL, array_keys($props)).'=?';
        $stmt = $this->prepare("update $tbl set $setString where $pivotField=?");
        //bind values in the set statement
        $i = 1;
        foreach($props as $prop=>$val)
        {
            $stmt->bindValue($i, $val,$model->getBindType($prop));
            $i++;
        }
        //bind value in the where clause
        $stmt->bindValue($i, $pivotValue, $model->getBindType($pivotField));
        $result = $stmt->execute();
        $stmt->close();
        return $result !==false;

        return true;
    }

    /**
     * The primary key of the model is used to find the row in the table and delete it. if no primary key is defined
     * then we must  match all properies in the model to the columns in the table- if a row has all the same values
     * as the model then it is propbably same to delete it.
     * @param $model $model - a  class object that extends the Model class and  row we are about to delete from the table
     * @return bool- whether the delete succeed or failed
     */
    public function delete(Model $model)
    {
        //get the value of the primary key property
       // $pkVal = $model->pk();// get the value of the property that is the primary key no matter what the property name is
        $tbl = $model->getTableName();
        //$pkName = $model->getPKFieldName();
        $filters = array();

        //TODO deal with the situation if there is no primary key
        //if no primary key is defined then do the much more intensive filter
        if(empty($model->getPKFieldName()))
        {
            //loop through the properties of the model and create filters
            foreach($model as $propName=>$val)
            {
                $filters[] = new Filter($propName, $val);
            }
        }
        else{
            //add only filter to the array-a filter for the primary  key
            $filters[] = new Filter($model->getPKFieldName(), $model->pk());
        }
        $filtersString = implode(' AND ', $filters);
        //prepare the statement
        $stmt = $this->prepare("delete from $tbl where $filtersString");
        $i = 1;
        foreach($filters as $filterObj)//php storm does not provide autocomplete because it does not know what's in the filter array
        {
            $stmt ->bindValue($i, $filterObj->value, $model->getBindType($filterObj->fileName));
            $i++;
        }
        $result = $stmt->execute();
        $stmt->close();
        return $result !== false;
    }

    /**
     * this function will determine whether a given Model $model is in the database
     * uses PK field to compare if there is one, if not will use all attributes of the $model
     * @param $model - the model to check if it is in the database
     * @return bool - true if in database, false if not
     */
    public function exists(Model $model)
    {
        $find = false; //whether the $model is in database
        $modelArray = $this->selectSome($model, '',true, '', true ); //all rows in the database

        if(!empty($model->getPKFieldName()))
        {//there is a PK field, just check PK
            //check if PK of model is in db
            //For each model, in model array see if the id matches $model
            $pkFieldName = $model->getPKFieldName(); //get PK field name
            foreach($modelArray as $curModel) //loop through all Models form database
            {
                if($curModel->$pkFieldName === $model->pk()) //checks the PK value of passed in $model to Models from database
                {
                    $find = true;
                }
            }
        }
        else { //there is no PK either passed in $model PK  value is null or $model does not have a PK field
            $numberProp = count( get_object_vars($model)); //gets the number of properties the $model has
            //No PK field, have to check all columns to see if exists
            if ($numberProp != count(get_object_vars($modelArray[0]))) { //check the number of columns = number of properties
                return false;
            } else {
               // $find = false;
                foreach ($modelArray as $currentModel) { //loops through all Models from database
                    $i = 0;
                    foreach ($model as $modelProp => $modelValue) { //loops through each property/column
                        if ($currentModel->$modelProp === $modelValue) {//compares each Models column value to the corresponding values in passed in $model
                            $i = $i + 1; //if columns match add one

                        }
                    }
                    if ($i === $numberProp) { //if matching columns = total number of columns the $model exists in database
                        $find = true;
                    }
                }
            }

        }
        return $find;
    }


    /**
     * this function will update or insert a model in the database.
     * @param Model $model - a passed in Model object to either be added or updated in database
     * @return bool - whether the update/insert was successful
     */
    public function save($model)
    {
        //If model exists - update
        //else  - insert
        return   $this->exists($model) ? $this->update($model) : $this->insert($model);
    }


}






























