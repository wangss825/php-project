<?php
  
/**
 * User: cst207 Dan Liu, cst201 Dylan Anderson
 * Date: 10/17/2018
 * SERVER LOGIC BLOCK
 * The purpose of this class to be extended by Data objects
 * this class will act as an ABSTRACT class-should not be instantiated
 * this class will define certain  properties and functions that will be used by the DBObject
 *
 */

namespace DBAccess;


use HTMLTools\FormInput;
use HTMLTools\FormSelect;
use HTMLTools\FormButton;
use HTMLTools\FormTextArea;
use HTMLTools\InputType;

require_once 'Column.php';



class Model
{
    private $tableColumns;// array of column objects
    private $autoColumns;//assoc array of field names of columns that are defined as autoincrement
    private $pkFieldName;// stores the name of the property/column that acts as the primary key for the model/table
    private $displayLabels;

    private $errors;//array that will hold field validation errors
    public $input; //array to hold input type for each column in Model object


    /**
     * Creates a column object and adds it to the private array $tableColumn
     * @param string $fieldName-the column name within the table
     * @param string $type- the column type example(Integer, varchar, dataetime etc..)
     * @param integer $size- optional the size of the column only used for text typpe(varchar(size), nvarchar(size) etc..)
     * @param bool $isNullable-optional whether the column can contain null values-default false meaning that column is required
     * @param bool $isPK-optional whether the column is the parmary key for the table-default false
     * @param bool $isAuto-optional whether the column is autoincrementing only for integer type column-default false.
     */
    //protected function defineColumn($fieldName, $type, $size=null, $isNullable=false, $isPK=false, $isAuto=false)
    protected function addTableColumn($fieldName, $type, $size=null, $isNullable=false, $isPK=false, $isAuto=false)
    {
//        $col = new Column($fieldName, $type, $size=null, $isNullable=false, $isPK=false, $isAuto=false);
//        $this->tableColumns []= $col;

        $this->tableColumns[$fieldName] = new Column($fieldName, $type, $size, $isNullable, $isPK, $isAuto);
        if($isAuto)
        {
            $this->autoColumns[$fieldName] = $fieldName;
        }
        if($isPK)
        {
            $this->pkFieldName = $fieldName;
        }
    }

    /**
     * Assoc array of field name of autoincrementing columns
     */
    public function getAutoColumns()
    {
        return is_array($this->autoColumns) ? $this->autoColumns: array();
    }


    /**
     * @return boolstring- returns the name of the primary key column/property if no primary key then returns false
     */
    public function getPKFieldName()
    {
        return isset($this->pkFieldName) ? $this->pkFieldName: '';
    }

    /**
     * @return string- the sql that can be used to create the table with  columns and the class name as the table name
     */
//    public function getTableDefinition()
//    {
//        //only create the table if it does not already exist.
//        $sql = 'create table  IF NOT EXISTS '.$this->getTableName() .'('.PHP_EOL;
//
//        //ex23: finish the getTableDefinition function
//
//        $sql .= implode(','.PHP_EOL, $this->tableColumns) .')';// make a comma and new line separated string
//
//
////        foreach ($this->tableColumns as $tableColumn)
////        {
////            $sql .= $tableColumn . ',';
////        }
////        $sql = substr($sql,0, -1 );
////
////        $sql .= ')';
//
//        return $sql;
//    }
    public function getTableColumns()
    {
        //get tableColumn array if it is not empty, otherwise return an empty array
        $tableColumns = $this->tableColumns;
        return  empty($tableColumns) ? array() : $tableColumns;
    }


    public function getTableName()
    {
        return get_class($this);//use the class name as the table name
    }

    /**
     * @param $fieldName = column name to which the sqlite3 bind type is returned
     * @return mixed- sqlite3 bind type (integer, text, float, blob)
     *
     */

    public function getBindType($fieldName)
    {
        //check that the columns array has items and has the specified column the return the bind type
        return is_array($this->tableColumns) &&  isset($this->tableColumns[$fieldName]) ? $this->tableColumns[$fieldName]->bindType : SQLITE3_TEXT;
    }

    public function pk($value=null)
    {
        $pk= $this->getPKFieldName();
        //it is possible that the developer may not declare the columns (defineColumn/addTableColumn) and no primary key
        if(empty($pk))
        {
            return false;// return false if the developer did not define a primary key
        }
        //if the developer calls the function with no value then just return the value otherwise set the value;
        if(!is_null($value))
        {
            $this->$pk = $value;
        }
        return $this->$pk;//return the value of the field that acts as the primary key


    }

//    public function getDisplayLabel($prop)
//    {
//        //if the property is not empty, show the property' value in the displayLabels array, otherwise show the property from the displayLabels array
//        return  empty($this->displayLabels[$prop])  ? $prop:$this->displayLabels[$prop];
//    }
    protected function addDisplayLabel($prop, $labelprop)
    {
        //set the prop's value as the $showProp, and add them to the displayLabels array.
        $this->displayLabels[$prop] = $labelprop;

    }
    /**
     * @param $prop-the label property in the $displayLabels;
     * @return  label name if it has been set, otherwise use the property name
     */

    public function getDisplayLabel($prop)
    {
        //if the property is not empty, show the property' value in the displayLabels array, otherwise show the property from the displayLabels array
        //return  empty($this->displayLabels[$prop]) ? $prop:$this->displayLabels[$prop];
        return is_array($this->displayLabels) && isset($this->displayLabels[$prop]) ? $this->displayLabels[$prop]: $prop;
    }

    /**
     * this function will add errors to the error array if the value in the checked property is invalid
     * @param $fieldName- the name of the property/field that is associated to the error
     * @param $isValid- whether or not the value in the property  meets  validation checks
     * @param string $msg- a string format that will use the friendly display label in  the error message
     * @return bool - passes through the isValid parameter
     */
    public function checkProperty($fieldName, $isValid, $msg="%s is invalid")
    {
        if(!$isValid)
        {
            //add the error message to the errors array using the friendly label name to display to the user
            $this->errors[$fieldName] = sprintf($msg, $this->getDisplayLabel($fieldName));
            //using the fieldname as the key prevents showing the user multiple errors on the same field/ property
        }

        return $isValid;
    }

    public function getErrorMessage($fieldName)
    {
        // just for ease of use- if developer has not called validate but calls getErrorMessage
        if(!is_array($this->errors))//if it 's not an array then validate has not been called
        {
            $this->validate();// call validate now to populate the errors array
        }
        // if the field/property has an error return the error message otherwise return empty string
        return   isset($this->errors[$fieldName])? $this->errors[$fieldName]: '';
    }

    public function validate()
    {
        $this->errors = array();//wipe any existing errors from the array
        $methods = get_class_methods($this);//$this will be the car object

        $validationResults= array();
        foreach ($methods  as $funcName)
        {
            //check if the function name begin with 'validate_'
            if(strpos($funcName, 'validate_')===0)
            {
                $validationResults[$funcName] = call_user_func([$this, $funcName]);//call /execute the function

            }
        }
        // return whether all the validation functions returned true- if even 1 function returns false the whole model is invalid
        return !in_array(false,$validationResults);//check that there is not false
    }

    /**
     * @param $name - The name of the property we want to add an input to
     * @param $type - the type of input to use based off InputType
     * @param string $value - OPTIONAL the value of the input
     * @param string $id - OPTIONAL the id for the input
     * @param string $displayName - OPTIONAL ONLY used if creating a button
     * @param array $optionItems - OPTIONAL ONLY used if creating a select - an array of items to be selected form in a select
     * @param string $selectedKey - OPTIONAL ONLY used if creating a select - the value attribute of the option that should show as selected
     * @param string $firstText - OPTIONAL ONLY used if creating a select - display text of the first option item-DEFAULT '....'
     * @param string $firstValue - OPTIONAL ONLY used if creating a select - value attribute of the first option tag-default 'no seledted'
     * @param string $rows - OPTIONAL ONLY used if creating a textarea - the number of rows for the textarea
     * @param string $col - OPTIONAL ONLY used if creating a textarea - the number of columns for the textarea
     * @param array $attributes - list of attributes to add to the input
     */
    public function addInput($name, $type, $value='', $id='', $displayName="", $optionItems=[], $selectedKey='0',$firstText="...", $firstValue='not selected',
                             $rows = "", $col = "", $attributes=array())
    {
        $useType = InputType::bindFormType($type); //gets the bind type for the passed in $type parameter
        $input = null;
        switch($useType)
        {
            case "HTMLTools\FormInput": //if the bind type is Input
                $input = new FormInput($name, $type, $value, $id, $attributes); //creates new FormInput
                break;
            case "HTMLTools\FormSelect": //if bind type is Select
                if(!empty($optionItems)) //check that the $optionItems has something in it
                {
                    $input = new FormSelect($name, $optionItems, $selectedKey, $id, $firstText, $firstValue, $attributes); //Creates a new FormSelect
                }
                break;
            case "HTMLTools\FormButton": //if bind type is Button
                if(!empty($displayName)) { //checks that a displayName was passed in
                    $input = new FormButton($name, $type, $displayName, $value, $id, $attributes); //creates a new FormButton
                }
                break;
            case "HTMLTools\FormTextArea": //If bind type is TextArea
                $input = new FormTextArea($name, $type, $rows, $col, $value, $id, $attributes); //creates new FormTextArea
                break;
        }

        empty($input) ? :$this->input[$name] = $input; //adds the created $input to the array

    }

    /**
     * @param $name - the name of the column that you want to get the input for
     * @return mixed
     */
    public function getInput($name)
    {
        return $this->input[$name];
    }

}





















