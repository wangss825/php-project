<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 11/23/2018
 * SERVER LOGIC BLOCK
 */

namespace HTMLTools;

 
//require_once '../DBAccess/DBObject.php';
require_once 'HTMLBase.php';

use DBAccess\Model;

class DisplayForm extends HTMLBase
{
    /**
     * @param Model $model - the model to be used to create the form
     * @param bool $showError - whether to show an error or not
     * @param array $values - an array of properties/columns for the Model object
     * @param array $formAttr - any additional attributes for the form tag
     * @param array $inputAttr - any additional attributes for the input tag(s)
     * @param array $labelAttr - any additional attributes for the label tag(s)
     * @param array $errorAttr - any additional attributes for the error(s)
     */
    public function __construct($model, $showError, $values=array(), $formAttr=array(), $inputAttr=array(), $labelAttr=array(), $errorAttr=array())
    {
        //starts the form tag
        $this->startTag('form', $formAttr);
        //if $values is not empty
        if(!empty($values))
        {
            //make sure only properties/columns the Model has are used, could be some extras
            $fields = array_intersect_key($values, get_object_vars($model));
        }
        else
        {
            //if empty just use the Model properties
            $fields = get_object_vars($model);
        }
        $autoField = $model->getAutoColumns();
        if($autoField !=array())
        {
            foreach($autoField as $attr=>$val)
            {
                unset($fields[$attr]);
            }
        }
        //go through the fields and
        foreach($fields as $field=>$value)
        {
            $model->$field = $value;

            $input= self::getFormInput($model, $field, $inputAttr, $labelAttr, $errorAttr, $showError );
            $this->html .= $input->getHtml();
        }

        $type = InputType::SUT;
        //add a submit button
        $this->html .=<<<EOT
        <input type=$type  value="Submit"/>
EOT;
        //close the form tag
        $this->closeTag();

    }


    /**
     * @param Model $model - model to use
     * @param $fieldName - the field name you want to use
     * @param array $inputAttr - add attributes to the input
     * @param array $labelAttr - add attributes to label
     * @param array $errorAttr - add attributes to error
     * @param bool $showError - whether to show the errors or not
     * @return mixed - a FormBase object with labels, errors, as added
     */
    static public function getFormInput($model,$fieldName, $inputAttr=array(), $labelAttr=array(), $errorAttr=array(), $showError )
    {
        $model->getInput($fieldName)->addLabel($fieldName,'',$labelAttr);
        $model->getInput($fieldName)->addError( $showError,$model->getErrorMessage($fieldName), $errorAttr);
        return $model->getInput($fieldName);
    }




}