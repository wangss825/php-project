<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * * Date: 11/5/2018
 * SERVER LOGIC BLOCK
 */

namespace HTMLTools;
require_once 'HTMLBase.php';


class FormBase extends HTMLBase
{
    protected $name;// required attribute of the input tag(needed to post data to the server)
    protected $hasLabel;//boolean to prevent developer from add more than onbe label for an input tag



    /**
     * Starts the form tag with the attributes name and id and any additional attributes
     * @param string $tag - the  element name/ tag name should be one of the following input, select, textarea, button
     * @param  string $name- the name attribute of the tag<input name="{name}" ...>
     * @param string $id--OPTIONAl the id attribute of the tag<input name="{id}" ...> --default uses name parameter
     * @param array $attributes- mixed array of  attribute names and values-DEFAULT empty array
     */
    public function __construct($tag, $name, $id='', $attributes=array() )
    {
        $attributes['name'] = $name;
        $this->name = $name;
        $attributes['id'] = empty($id)? $name: $id;// if no id is supplied use the name para as id as well
        $this->startTag($tag, $attributes );
    }


    public function addLabel($label,$for="",$attributes=array())
    {
        //don't add another label if it already has a label
        if(!$this->hasLabel) {

            //TODO deal with $label     ---  /1 mark
            $label = htmlentities($label);

            //TODO deal with $for       ---  /1 mark

            $for = empty($for) ? $this->id: $for;

            //convert attributes array to attribute string
            $attrString = $this->attributesToString($attributes);

            //define the label
            $labelHTML = <<<EOT
<label for="$for" $attrString>$label</label>

EOT;
            //put label tag html to existing html
            $this->html = $labelHTML . $this->html;

            //once we add the label set the tracking variable to true
            $this->hasLabel = true;
        }

    }

    /**
     * appends a span tag with a message after the html generated in the constructor
     * @param boolean $showError - can be used with validation logic to determine whether to display the error message
     * @param string $errorMessage - text to display in the span tag - this function cleans the text for safe display
     * @param string $cssClassName - OPTIONAL the class attribute of the span tag
     * * @param array $attributes- mixed array of  attribute names and values-DEFAULT empty array
     */
    public function addError($showError, $errorMessage,$attributes=array())
    {

        if($showError){

            //clean message text before adding to html
            $errorMessage=htmlentities($errorMessage);
            $attrString = $this->attributesToString($attributes);

            //append span tag html to existing html
            $this->html.= <<<EOT
        <span $attrString>$errorMessage</span>


EOT;

        }

    }


}