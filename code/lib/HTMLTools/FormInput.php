<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 11/5/2018
 * SERVER LOGIC BLOCK
 */

namespace HTMLTools;

require_once 'HTMLBase.php';
require_once 'FormBase.php';


class FormInput extends FormBase
{


        /**
    * constructor extends HTMLForm Input and uses the parent to render the input element/tag
    * @param string $name - the name attribute of the input tag
    * @param string $type - the type attribute of the input tag
    * @param string $value - the value attribute of the input tag - this function cleans the text for safe display
     * @param string $id - OPTIONAL id attribute of the input tag - if empty the name parameter is used as the id
     * @param bool $required - OPTIONAL specifies whether or not to add the 'required' attribute to the input tag
     * @param string $regex - OPTIONAL specifies the 'pattern' attribute (pattern="regex") - if empty the 'pattern' attribute is not added to the input tag
     * @param array	$attributes - OPTIONAL mixed array of (key-value) extra attributes to add to the input tag
     */
	public function __construct( $name, $type, $value='',$id='', $attributes=array())
    {
        if($type !='text')
        {
            $attributes['type'] = $type;
        }
        //$attrString = "";
        if(!empty($value))
        {
            $attributes['value']= htmlentities($value);
        }
        parent::__construct('input', $name, $id, $attributes);
        //$this->closeTag();

//        $attributes['type'] = $type;
//        //clean value because it will most likely com from the $_POST(meaning it comes from the user input)
//        $attributes['value'] = empty(htmlentities($value)) ?"": $value;
//        $attributes['required'] = empty($require)? "":"required";
//        $attributes['pattern']  = empty($regex) ? "":$regex;
//
//        foreach ($attributes as $key=>$val) {
//            if(empty($val))
//            {
//                unset(attributes[$key]);
//            }
//            elseif(is_numeric($key)|| $key==$val)
//            {
//                $attrString .="$val ";
//            }
//            else{
//                $attrString .= sprintf('%s ="%s"', $key, $val);
//            }
//        }
//        parent::__construct('input', $name, $id, $attrString);


    }


}