<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 11/5/2018
 * SERVER LOGIC BLOCK
 */

namespace HTMLTools;
require_once 'HTMLBase.php';

class FormSelect extends FormBase
{

    /**
     * HTMLFormSelect constructor.
     * @param $name- the name attribute of trhe select tag
     * @param $optionItems- the array of  key-value for the option tags
     * @param string $selectedKey- the value attribute of the option that should show as selected
     * @param string $id- the OPTIONAL id attribute of the select tag
     * @param string $firstText- the OPTIONAL display text of the first option item-DEFAULT '....'
     * @param string $firstValue-the OPTIONAL value attribute of the first option tag-default 'no seledted'
     * @param array$extraAtrtibutes- OPTIONAL array of attribute names and values - DEFAULT empty array
     */

//    public function __construct(string $tag, string $name, string $id = '', array $attributes = array())
//    {
//        parent::__construct($tag, $name, $id, $attributes);
//    }

    function __construct( $name,$optionItems, $selectedKey = "0", $id="", $firstText="...", $firstValue='not selected', $atrtibutes=array() )
    {

        //call the parent to start the select table EX28
        parent::__construct('select', $name, $id='', $attributes=array());


        //add first item if firstValue or firsttext are not empty
        if(!empty($firstValue) || !empty($firstText))
        {
            //add array item to the front of an existing array
            $optionItems = array($firstValue=>$firstText) + $optionItems;
        }

//        $firstItem = $firstValue !== ""   ? "firstText": "" ;
//
//        $this->html .= <<<EOT
//         <option  value="firstValue">$firstItem </option>
//
//EOT;
        foreach ($optionItems as $optValAttr=>$displayText) {

            $selected = $optValAttr ===$selectedKey ? "selected" : "";
            $this->html .=<<<EOT
            <option  value="$optValAttr"  $selected> $displayText </option>

EOT;

        }
        $this->closeTag();

    }


}