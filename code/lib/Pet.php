<?php
/**
 * Created by PhpStorm.
 * User: CST207 Dan liu, CST201 Dylan Anderson
 * Date: 2018-11-25
 * Time: 4:15 PM
 */
require_once 'DBAccess\Model.php';
require_once 'DBAccess\Column.php';
use DBAccess\ColumnType;
class Pet extends \DBAccess\Model
{


    public $id;

    /**
     * @return bool- if the pet's id is valid
     */
    public function validate_id()
    {
        return $this->checkProperty('id', is_null($this->id) || is_int($this->id), 'The %s must be a number or blank');
    }
    public $petName;

    /**
     * @return bool-check if the petName is valid
     */
    public function validate_petName()
    {
        return $this->checkProperty('petName', !empty($this->petName) || strlen($this->petName)<=40, 'The %s must 40 characters or less');
    }
    public $petBirthday;

    /**
     * @return bool- check if the petBirthday is valid
     */
    public function validate_petBirthday()
    {
        return $this->checkProperty('petBirthday', !empty($this->petBirthday) ||($this->petBirthday)<=100, 'The %s must 100 characters or less');
    }

    public $petSpecies;

    /**
     * @return bool-check if the petSpecies is valid
     */
    public function validate_petSpecies()
    {
        return $this->checkProperty('petSpecies',strlen($this->petSpecies)<=40, 'The %s must 40 characters or less');
    }
    public $ownerId;

    /**
     * @return bool-check if the ownerId is valid
     */
    public function validate_ownerId()
    {
        return $this->checkProperty('ownerId', is_int($this->ownerId) && $this->ownerId !== 0, 'The %s must be a number or blank');
    }

    /**
     * Pet constructor.
     * @param $id
     * @param $petName
     * @param $petBirthday
     * @param $petSpecies
     * @param number $ownerId
     */
    public function __construct($id=null, $petName=null, $petBirthday=null, $petSpecies=null, $ownerId=null)
    {
        $this->id = $id;
        $this->petName = $petName;
        $this->petBirthday = $petBirthday;
        $this->petSpecies = $petSpecies;
        $this->ownerId = is_numeric($ownerId) ? $ownerId : null;


        //add the attributes to the table column.
        $this->addTableColumn('id', ColumnType::INT, null, false, true, true);
        $this->addTableColumn('petName', ColumnType::VRC, 40);
        $this->addTableColumn('petBirthday', ColumnType::VRC, 100);
        $this->addTableColumn('petSpecies', ColumnType::VRC, 40);
        $this->addTableColumn('ownerId', ColumnType::INT);



        //add the label when show the pet
        $this->addDisplayLabel('id ',"ID");
        $this->addDisplayLabel('petName ',"Pet Name");
        $this->addDisplayLabel('petBirthday ',"Pet Birthday");
        $this->addDisplayLabel('petSpecies ',"Pet Species");
        $this->addDisplayLabel('ownerId ',"Owner ID");


        //add every attribute to the input type form
        $this->addInput('id',\HTMLTools\InputType::TXT);
        $this->addInput('petName',\HTMLTools\InputType::TXT);
        $this->addInput('petBirthday',\HTMLTools\InputType::DAT);
        $this->addInput('petSpecies',\HTMLTools\InputType::TXT);
        $this->addInput('ownerId',\HTMLTools\InputType::NUM);




    }


}