<?php
/**
 * User: CST207 Dan liu, CST201 Dylan Anderson
 * Date: 11/252018
 * SERVER LOGIC BLOCK
 */

require_once '../lib-bak/Pet.php';
require_once '../lib-bak/Member.php';
require_once '../lib-bak/DBAccess/Column.php';
use DBAccess\ColumnType;
require_once '../lib-bak/DBAccess/ColumnType.php';
require_once '../lib-bak/DBAccess/DBObject.php';
require_once  '../lib-bak/DBAccess/Filter.php';
require_once '../lib-bak/HTMLTools/FormButton.php';
require_once '../lib-bak/HTMLTools/FormSelect.php';
require_once '../lib-bak/HTMLTools/HTMLFormType.php';
require_once '../lib-bak/HTMLTools/InputType.php';
require_once '../lib-bak/HTMLTools/DisplayForm.php';
require_once '../lib-bak/HTMLTools/FormInput.php';

use HTMLTools\InputType;
use HTMLTools\FormInput;
use DBAccess\Filter;
//start session
session_start();

//no ownerId found, go the the login page
empty($_SESSION['ownerId']) ?  header('Location: login.php') : null;
$isPosted = $_SERVER["REQUEST_METHOD"] == "POST";

//get the pet's all properties from the user's input
$postid = empty($_POST['id'])? null: htmlentities($_POST['id']);
$postpetName = empty($_POST['petName'])? '': htmlentities($_POST['petName']);
$postpetBirthday = empty($_POST['petBirthday'])? '': htmlentities($_POST['petBirthday']);
$postpetSpecies = empty($_POST['petSpecies'])? '': htmlentities($_POST['petSpecies']);
$postownerId=  $_SESSION['membershipType'] === 'admin' ? empty($_POST['ownerID'])? null: intval( $_POST['ownerId']) : empty($_POST['ownerID'])? $_SESSION['ownerId']: intval( $_POST['ownerId']) ;

//$postownerId=  $_SESSION['membershipType'] === 'admin' ? empty($_POST['ownerId']) ? null : intval($_POST['ownerId']) :empty($_POST['ownerId']) ? $_SESSION[''] : intval($_POST['ownerId']) ;

//create Pet object from posted data
$pet= new Pet($postid,$postpetName,$postpetBirthday,$postpetSpecies,$postownerId);

//if Pet object is valid-save data to the database
$db= new DBAccess\DBObject('../db/cst201cst207pets.db');
$db->createTable($pet);

$insertable = true;

$result = $db->selectSome($pet, [new  Filter('ownerid', $pet->ownerId)]);
if($_SESSION['membershipType']=='free' )
{
    //for free user, if the pets in the database is less than 2, they can add more more pets
    count($result)<2 ? $insertable = true : $insertable = false;

}
else if($_SESSION['membershipType']=='paid' )
{
    //for paid user, if the pets in the database is less than 5, they can add more pets
    count($result)<5 ? $insertable = true : $insertable = false;
}
if($isPosted && $pet->validate())
{
    //for admin, they can add whatever they want
    $db= new DBAccess\DBObject('../db/cst201cst207pets.db');
    $db->insert($pet);
}


//if($insertable)
//{
    //list all the pets belong to the user
    //header('Location: list-pet.php');
    //header('Location: add-pet.php');
//}

//get all the pets from the user's input to the pet list
$petForm = new \HTMLTools\DisplayForm($pet, $isPosted && !$pet->validate(), ['id'=>$postid,'petName'=>$postpetName ,'petBirthday'=>$postpetBirthday,'petSpecies'=>$postpetSpecies,
    'ownerId'=>$postownerId], ['action'=>'#', 'method'=>'POST']);



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Member Registration </title>
</head>
<body>
<style>
    label {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }
    input, select{
        display: block;
        float: left;
        margin: 5px;
        width: 200px;
    }
    input[type='submit'] {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }
</style>

<!--navigation list-->
<li><a href="login.php">Logout</a></li>
<li><a href="list-pet.php">See my Pets</a></li>
<li><a href="add-pet.php">Add a Pet</a></li>
<h1>Pet Registration </h1>
<div>
<!--    if the user have reached the maximium pets number, show the messages-->
    <?php if($insertable == false ) { ?>
    <h2>you have the maximium pets in the system </h2>
</div>
<?php } else { ?>
<!--    show the add-pet form-->
    <div><?php $petForm->render() ?></div>

<?php } ?>
<form action="list-pet.php">
    <button>Back</button>
</form>


</body>
</html>

