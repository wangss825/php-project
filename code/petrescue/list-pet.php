<?php
/**
 * Created by PhpStorm.
 * User: CST207 Dan liu, CST201 Dylan Anderson
 * Date: 2018-11-25
 * Time: 5:08 PM
 */

require_once '../lib-bak/Pet.php';
require_once '../lib-bak/Member.php';
require_once '../lib-bak/DBAccess/Column.php';
use DBAccess\ColumnType;
require_once '../lib-bak/DBAccess/ColumnType.php';
require_once '../lib-bak/DBAccess/DBObject.php';
require_once  '../lib-bak/DBAccess/Filter.php';
require_once '../lib-bak/HTMLTools/FormTextArea.php';
require_once '../lib-bak/HTMLTools/FormButton.php';
require_once '../lib-bak/HTMLTools/FormSelect.php';
require_once '../lib-bak/HTMLTools/HTMLFormType.php';
require_once '../lib-bak/HTMLTools/InputType.php';
require_once '../lib-bak/HTMLTools/DisplayForm.php';
require_once '../lib-bak/HTMLTools/FormInput.php';
require_once '../lib-bak/HTMLTools/DisplayTable.php';

use HTMLTools\InputType;
use HTMLTools\FormInput;

//start session
session_start();
//if the session has no ownerID stored, go to the login page
empty($_SESSION['ownerId']) ?  header('Location: login.php') : null;
$db= new DBAccess\DBObject('../db/cst201cst207pets.db');
//get the petName from the user's input
$petName = empty($_POST['search']) ? null : $_POST['search'];
$pet = new Pet();
if(!empty($petName) )
{
    // make it can be searched for all kind of the field name, also can search part of the string about the pet's properties
    //loop through every properties of the pet class
    $filters = array();
    foreach ($pet as $prop=>$val)
    {
       $filters[$prop] =  new \DBAccess\Filter($prop, "%$petName%", 'LIKE');
    }
    $result = $db->selectSome($pet, $filters, false, $pet->petName, true);
    //display the pets in the pets list
    $petList = new \HTMLTools\DisplayTable($result);
}
//for free or paid user, show the pets list.
else if($_SESSION['membershipType']=='free'|| $_SESSION['membershipType']=='paid' )
{
    $result = $db->selectSome($pet, [new \DBAccess\Filter('ownerId', $_SESSION['ownerId'])], true, $pet->petName, true);
    $petList = new \HTMLTools\DisplayTable($result);
}

//for admin, show the petlist
else if($_SESSION['membershipType']=='admin' )
{
    $result = $db->selectSome($pet);
    $petList = new \HTMLTools\DisplayTable($result);

}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List Pet</title>
</head>
<body>

<!--navigation list-->

<li><a href="login.php">Logout</a></li>
<li><a href="list-pet.php">See my Pets</a></li>
<li><a href="add-pet.php">Add a Pet</a></li>

<h1>List Pet</h1>
<!--show the petlist-->
 <?php $petList->render() ?>

<!--turn to the add pet page-->
<form action="add-pet.php">
    <button>Add Pet</button>
</form>

<!--admin can search the pet-->
<?php if($_SESSION['membershipType']=== 'admin') {?>
<form method="POST">
    <input type="search" name="search" />
    <button type="submit">Search</button>
</form>
<?php } ?>
</body>
</html>