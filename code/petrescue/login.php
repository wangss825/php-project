<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 11/252018
 * SERVER LOGIC BLOCK
 */


require_once '../lib-bak/Member.php';
require_once '../lib-bak/DBAccess/Column.php';
use DBAccess\ColumnType;
require_once '../lib-bak/DBAccess/ColumnType.php';
require_once '../lib-bak/DBAccess/DBObject.php';
require_once '../lib-bak/HTMLTools/FormTextArea.php';
require_once '../lib-bak/HTMLTools/FormButton.php';
require_once '../lib-bak/HTMLTools/FormSelect.php';
require_once '../lib-bak/HTMLTools/HTMLFormType.php';
require_once '../lib-bak/HTMLTools/InputType.php';
require_once '../lib-bak/HTMLTools/DisplayForm.php';
require_once '../lib-bak/HTMLTools/FormInput.php';

use \DBAccess\Filter;
use HTMLTools\InputType;
use HTMLTools\FormInput;

$isPosted = $_SERVER["REQUEST_METHOD"] == "POST"; //whether the page has been posted or not

$postEmail = empty($_POST['email'])? '': $_POST['email'];
$postPassword = empty($_POST['password'])? '': $_POST['password'];
$postmembershipType = empty($_POST['membershipType'])? '': $_POST['membershipType'];
//create member object from posted data
$member= new Member($postEmail,$postPassword,$postmembershipType); //create a new member based on posted data for $_Post

$notFound= true;

if($isPosted && $member->validate()) //if the form has been posted and the member is valid
{
    $db= new DBAccess\DBObject('../../db/cst201cst207pets.db');
    $members = $db->selectSome($member);//, [new Filter('email'), $member->email)]);
// we may get multiple results back- if the same email was used to register multiple times
//loop through all the results and use password_verify to see if  the user is autnenticated
    foreach ($members as $currMember) //go through each member in the database and see if the email and password match
    {
        if (password_verify($member->password, $currMember->password)) { //if a match is found, create a new session redirect to list-pet.php
            //session variables are often used to grant access to users once they are authenticate
            $_SESSION['member'] = ['email'=>$currMember->email, 'membershipType'=>$currMember->membershipType];
            $notFound = false;
            header('Location: list-pet.php');

        }
    }
}

//create the form
$memberForm = new \HTMLTools\DisplayForm($member, $isPosted && !$member->validate()  ,['email'=>$postEmail,'password'=>$postPassword,'membershipType'=>$postmembershipType], ['action'=>'#', 'method'=>'POST']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Member login</title>
</head>
<body>
<style>
    label {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }
    input, select{
        display: block;
        float: left;
        margin: 5px;
        width: 200px;
    }
    input[type='submit'] {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }

    span{
        display: block;
        float: left;
        margin: 5px;
    }
</style>

<h1>Member login</h1>
<div><?php if($isPosted  && $notFound) { ?>
    <h2> Sorry invalid email or password is incorrect</h2>
</div><?php } ?>
<div><?php $memberForm->render() ?></div>


</body>
</html>