<?php
/**
 * User: cst207 Dan Liu, cst 201 Dylan Anderson
 * Date: 11/252018
 * SERVER LOGIC BLOCK
 */

require_once '../lib-bak/Member.php';
require_once '../lib-bak/DBAccess/DBObject.php';
require_once '../lib-bak/HTMLTools/FormButton.php';
require_once '../lib-bak/HTMLTools/FormSelect.php';
require_once '../lib-bak/HTMLTools/InputType.php';
require_once '../lib-bak/HTMLTools/DisplayForm.php';
require_once '../lib-bak/HTMLTools/FormInput.php';

$isPosted = $_SERVER["REQUEST_METHOD"] == "POST"; //checks to see if page has been posted

//read in posted values and check if they are empty
$postEmail = empty($_POST['email'])? '': htmlentities($_POST['email']);
$postPassword = empty($_POST['password'])? '': htmlentities($_POST['password']);
$postmembershipType = empty($_POST['membershipType'])? '':htmlentities($_POST['membershipType']) ;
$postfirstName = empty($_POST['firstName'])? '':htmlentities($_POST['firstName']) ;
$postlastName= empty($_POST['lastName'])? '':htmlentities($_POST['lastName']) ;
$postaddress1 = empty($_POST['address1'])? '': htmlentities($_POST['address1']);
$postaddress2 = empty($_POST['address2'])? '': htmlentities($_POST['address2']);
$postcity = empty($_POST['city'])? '': htmlentities($_POST['city']);
$postprovince = empty($_POST['province'])? '':htmlentities($_POST['province']) ;

//create member object from posted data
$member= new Member($postEmail,$postPassword,$postmembershipType,$postfirstName,$postlastName,$postaddress1,$postaddress2,$postcity,$postprovince);

$db= new DBAccess\DBObject('../../db/cst201cst207pets.db');
$db->createTable($member);

//create the form for entering data to create a new user
$memberForm = new \HTMLTools\DisplayForm($member, $isPosted && !$member->validate(), ['email'=>$postEmail,'password'=>$postPassword,'membershipType'=>$postmembershipType,
    'firstName'=>$postfirstName, 'lastName'=>$postlastName, 'address1'=>$postaddress1,'address2'=>$postaddress2,'city'=>$postcity,'province'=>$postprovince],
    ['action'=>'#', 'method'=>'POST'] );

//if the page is posted and the member is valid insert the user into the database
if($isPosted && $member->validate())
{
    //has the password before we save it to the database- but we just invalid our object by changing as value after validating
    //$member->password = password_hash($member->password, PASSWORD_DEFAULT);// produces has of about 60 characters
    $member->sethashedPassword($member->password);
    $result = $db->insert($member);
    header('Location: login.php');
}





?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Member Registration </title>
</head>
<body>
<style>
    label {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }
    input, select{
        display: block;
        float: left;
        margin: 5px;
        width: 200px;
    }
    input[type='submit'] {
        display: block;
        float: left;
        clear: left;
        margin: 5px;
        width: 200px;
    }
    span{
        display: block;
        float: left;
        margin: 5px;
    }
</style>

<h1>Member Registration </h1>
<div>

</div>

<div><?php $memberForm->render() ?></div>

</body>
</html>
